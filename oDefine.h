#ifndef ODEFINE_H
#define ODEFINE_H

#include <QtWidgets>
#include <QRegExp>
#include <memory>
using std::unique_ptr;
using std::shared_ptr;
#include <unordered_map>
using std::unordered_map;
#include <string>
using std::string;

#endif // ODEFINE_H

